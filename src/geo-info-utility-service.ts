import { GeoInfoApi, GeoInfoApiLocation, GeoInfoApiResponse } from "./geo-info-api"
import { logger } from "./logger"

const randomRange = (range: number) => {
    const halfRange = range / 2;
    return parseFloat(((Math.random() * range) - halfRange).toFixed(4))
}

const sleep = (duration) => {
    logger.info(`Sleeping for ${duration}`)
    return new Promise<void>((resolve) => setTimeout(resolve, duration))
}

export class GeoInfoUtilityService {

    private _geoInfoApi: GeoInfoApi

    constructor() {
        this._geoInfoApi = new GeoInfoApi()
    }

    public generateRandomLocations(count: number): GeoInfoApiLocation[] {
        const locations: GeoInfoApiLocation[] = []
        for (let i = 0; i < count; i++) {
            locations.push({
                lat: randomRange(120), // -60 to 60 (Avoid poles where no sunrise!)
                long: randomRange(360) // -180 to 180
            })
        }
        return locations
    }

    public async getGeoInfoThrottled(
        locations: GeoInfoApiLocation[],
        batchSize: number,
        batchDelay: number
    ): Promise<GeoInfoApiResponse[]> {

        const results: GeoInfoApiResponse[] = []
        const length = locations.length
        let index = 0

        while (index < length) {
            logger.info(`fetching ${index} to ${index + batchSize - 1}`)
            const batchResults = await this._geoInfoApi.get(locations.slice(index, index + batchSize))
            results.push(...batchResults)
            index += batchSize
            if (index < length) await sleep(batchDelay) // Don't sleep on last loop
        }
        return results
    }

    public pickEarliestSunrise(responses: GeoInfoApiResponse[]): GeoInfoApiResponse {
        const sorted = responses.sort((a: GeoInfoApiResponse, b: GeoInfoApiResponse) => {
            return a.sunrise.getTime() - b.sunrise.getTime()
        })
        return sorted[0];
    }

}
