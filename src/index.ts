import { GeoInfoApiResponse } from "./geo-info-api"
import { GeoInfoUtilityService } from "./geo-info-utility-service"

const dayLengthInHours = (x: number) => (x / 60 / 60).toFixed(3)
const formatGeoInfo = (x: GeoInfoApiResponse) => {
    return `sunrise: ${x.sunrise.toDateString()} ${x.sunrise.toTimeString()} country: ${x.countryName} day-length: ${dayLengthInHours(x.dayLength)} Hours`
}

export default async function () {

    const geoUtil = new GeoInfoUtilityService()

    const locations = geoUtil.generateRandomLocations(100)
    const geoInfo = await geoUtil.getGeoInfoThrottled(locations, 5, 5000)
    const earliest = geoUtil.pickEarliestSunrise(geoInfo)

    console.log("\nResults\n-------")
    geoInfo.forEach((x) => console.log(formatGeoInfo(x)))
    console.log("\nEarliest Sunrise:\n----------------")
    console.log(earliest ? formatGeoInfo(earliest) : "no results")
}