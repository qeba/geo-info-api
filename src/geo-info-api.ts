import { SunriseSunsetApi, SunriseSunsetApiResponse } from "./client-apis/sunrise-sunset-api"
import { GeoNamesApi, GeoNamesApiCountryNameResponse } from "./client-apis/geo-names-api"
import { logger } from "./logger"

export type GeoInfoApiLocation = {
    lat: number,
    long: number
}

// Use union type to compose response
export type GeoInfoApiResponse = GeoInfoApiLocation & SunriseSunsetApiResponse & GeoNamesApiCountryNameResponse

export class GeoInfoApi {

    private _sunsetSunriseApi: SunriseSunsetApi
    private _geoNamesApi: GeoNamesApi

    constructor() {
        this._sunsetSunriseApi = new SunriseSunsetApi()
        this._geoNamesApi = new GeoNamesApi()
    }

    private async _getSingle(location: GeoInfoApiLocation): Promise<GeoInfoApiResponse> {
        try {
            const results = await Promise.all([
                this._geoNamesApi.getCountryName(location.lat, location.long),
                this._sunsetSunriseApi.get(location.lat, location.long)
            ])
            const combinedResult = Object.assign({}, location, results[0], results[1])
            return combinedResult
        } catch (ex) {
            logger.error(`Failed to get Geo Information for ${location.lat}, ${location.long}`, ex)
            return null
        }
    }

    public async get(locations: GeoInfoApiLocation[]): Promise<GeoInfoApiResponse[]> {
        const results = await Promise.all(locations.map((l) => this._getSingle(l)))
        return results.filter((x) => x != null) //Filter any null results from failed API calls
    }

}