import fetch from "node-fetch"
import { logger } from "../logger"

const hasResults = (resp) => resp && resp.status === "OK" && resp.results

export type SunriseSunsetApiResponse = {
    sunset: Date
    sunrise: Date
    dayLength: number
}

export class SunriseSunsetApi {

    public async get(lat: number, long: number): Promise<SunriseSunsetApiResponse> {

        const url = `https://api.sunrise-sunset.org/json?lat=${lat}&lng=${long}&formatted=0`
        const response = await fetch(url)
        const jsonData = await response.json()
        if (hasResults(jsonData)) {
            const { results } = jsonData;
            return {
                sunrise: new Date(results.sunrise),
                sunset: new Date(results.sunset),
                dayLength: results["day_length"]
            }
        } else {
            throw new Error(`Sunrise Sunset API returned no results for lat:${lat}, long:${long}`)
        }

    }

}