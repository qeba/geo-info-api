import fetch from "node-fetch"

const hasCountryName = (resp) => resp && resp.geonames && resp.geonames.length

export type GeoNamesApiCountryNameResponse = {
    countryName: string
}

export class GeoNamesApi {

    public async getCountryName(lat: number, long: number): Promise<GeoNamesApiCountryNameResponse> {
        const url = `http://ws.geonames.org/findNearbyPlaceName?lat=${lat}&lng=${long}&type=json&username=leighwilts`
        const response = await fetch(url)
        const jsonData = await response.json()
        const result: GeoNamesApiCountryNameResponse = {
            countryName: "Unknown"
        }
        if (hasCountryName(jsonData)) {
            result.countryName = jsonData.geonames[0].countryName
        }
        return result
    }

}