export class logger {
    public static error(message: string, error?: Error) {
        console.log(`\nERROR LOG: ${message}\nDetails: ${error && error.message}`)
    }

    public static info(message: string) {
        console.log(`\nINFO LOG:${message}`)
    }
}